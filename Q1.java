
public class Q1 {

	public static void main(String[] args) {
		
		//1) Na expressão a seguir, para qualquer valor boolean de b, qual o resultado de c?
		boolean c;
		boolean b = false; // ou true;
		System.out.println(c = b ? !b : b);//(false)
		 
		
		//A única variável inicializada é b. c não recebe b. Operador ternário escolhe a segunda opção(else). Resultado False para qualquer valor 
		//de b pois c não recebe b.  
	
	}
}

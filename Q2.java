//2) Qual o padrão de projeto utilizado no trecho de código escrito em Java abaixo? R: Singleton

public class A {
	public static final A INSTANCE = new A();
	
	private A() {
	
	}
	
	public static A getInstance() {
	return A.INSTANCE;
	}
}

//Para saber o valor mais próximo de 0, utilize a chamada tipoVar nomeVar = Solution.closesToZero(array);
//O método closesToZero() recebe um array de qualquer tamanho.
public class Q3 {
	public static void main(String[] args) {
 
		double[] ts = {7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7};
		double result = Solution.closesToZero(ts);
		System.out.println("Result: " + result);
		
	}

	static class Solution{

		static double positive(double[] ts){
			double menorPositivo = ts[0];
			for(int i = 0; i < ts.length; i++){
				if(ts[i]>0 && ts[i] <= menorPositivo){
				menorPositivo = ts[i];
				}
			}
			return menorPositivo;
		}
		
		static double negative(double[] ts){
			double menorNegativo = ts[0];
			
			
			for(int i = 0; i < ts.length; i++){
				if(ts[i] < 0 && ts[i] <= menorNegativo){
					menorNegativo = ts[i];
				}
			}
			for(int i = 0; i < ts.length; i++){
				if(ts[i] < 0 && ts[i] >= menorNegativo){
					menorNegativo = ts[i];
				}
			}
			return menorNegativo;
		}
		
		static double closesToZero(double[] ts ){
			try{
				double menorPositivo = positive(ts);
				double menorNegativo= negative(ts);
				double result;
				if(menorNegativo + menorPositivo > 0){
				result = menorNegativo;
				} else if(menorNegativo + menorPositivo == 0){
					result = menorPositivo;
				}else{
					result = menorPositivo;
				}
				
				 return result;
				}catch(Exception e){
					return 0;
				}
					
			}
	}
}	
